  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($picture) {
      print $picture;
    }?>
    <?php  if ($page == 0) { ?>
      <div class="nodetitle">
        <div class="date">
          <div class="number">
            <?php print format_date($node->created, 'custom', 'd'); ?>
          </div>
          <div class="month">
            <?php print format_date($node->created, 'custom', 'M'); ?>
          </div>
        </div>
        <h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2>
      </div>
    <?php  }; ?>
    <div class="content"><?php print $content?></div>
    <div class="meta">
      <?php if ($terms) { ?><span class="taxonomy"><?php print t("Archived in");?> <?php print $terms?></span><?php }; ?>
      <?php if ($links) { ?><span class="link"><?php print $links?></span><?php }; ?>
    </div>
  </div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<title><?php print $head_title ?></title>
<?php print $head ?>
<?php print $styles ?>
<?php print $scripts ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body>
<div id="center">
  <div id="header">
    <?php if($logo) { ?><span id="site-logo"><a href="<?php print $front_page ?>" title="<?php print $site_name ?>"><img src="<?php print $logo ?>" alt="<?php print $site_name ?>" /></a></span><?php } ?>
    <?php/* if($site_name || $site_slogan) {?><div id="site-info"> <!--SITE INFO-->
    <?php if($site_name) { ?><span id='site-name'><a href="<?php print $front_page ?>" title="<?php print $site_name ?>"><?php print $site_name ?></a></span><?php } ?>
    <?php if($site_slogan) { ?><span id='site-slogan'><?php print $site_slogan ?></span><?php } ?>
    </div> <!--END SITE INFO--><?php }*/?>
    <?php if (isset($secondary_links)) { ?>
    <div id="secondary">
      <?php print theme('links', $secondary_links) ?>
    </div>
    <?php } ?> <!--END SECONDARY-->
    <?php if($header){?>
    <div id="header_content">
      <?php print $header; ?>
    </div>
    <?php } ?>
  </div> <!--END HEADER-->
  <div id="primary">
    <?php if ($search_box) :?>
      <div class="searchform">
        <?php print $search_box; ?>
      </div>
    <?php endif; ?> <!--END SEARCH BOX-->
    <?php if (isset($primary_links)) { ?>
      <div id="menu">
      <?php print theme('links', $primary_links) ?>
      </div>
    <?php } ?>
  </div> <!--END PRIMARY-->
  <div id="column">
    <!--START SIDEBAR-->
    <div id="sidebar">
      <?php if ($sidebar) { ?>
        <?php print $sidebar ?>
      <?php } ?>
    </div>
    <!--END SIDEBAR-->
    <div id="main">
      <?php if ($content_top){ ?><div id="content_top"><!--CONTENT_TOP-->
        <?php print $content_top; ?>
      </div><!--END CONTENT_TOP--><?php } ?>
      <?php print $help ?>
      <?php if ($show_messages) { print $messages; } ?>
      <?php if ($title) { ?>
        <h1 class="title"><?php print $title ?></h1>
      <?php } ?>
      <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
      <?php print $content; ?>
    </div> <!--END MAIN -->
    <div class="clean">&nbsp;</div>
  </div>
  <div id="footer">
    <?php if($footer_message){?><div id="footer_message"> <!--FOOTER MESSAGE-->
      <?php print $footer_message ?>
    </div> <!--END FOOTER MESSAGE--> <?php } ?>
    <div id="footer_logos">
      <?php print credits(); ?>
    </div>
  </div>
</div>
<?php print $closure ?>
</body>
</html>
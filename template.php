<?php
/*
  Do not include drupal's default style sheet in this theme !
*/
function phptemplate_stylesheet_import($stylesheet, $media = 'all') {
  if (strpos($stylesheet, 'misc/drupal.css') == 0) {
    return theme_stylesheet_import($stylesheet, $media);
  }
}

function credits(){
  return "<h5>by <a href=\"http://www.realizzazione-siti-vicenza.com\" title=\"Drupal development, E-commerce\">R</a> &amp; <a href=\"http://www.themes-drupal.org\" title=\"Drupal template design\">T</a></h5>";
}

?>